const
  src = 'src/',
  dist = 'dist/',
  css = {
    src: src + 'assets/scss/*.scss',
    dist: dist + 'assets/css/'
  },
  //Modules
  gulp = require('gulp'),
  newer = require('gulp-newer'),
  postcss = require('gulp-postcss'),
  sass = require('gulp-sass'),
  autoprefixer = require('autoprefixer')({
    browsers: ['last 2 versions', '> 2%']
  }),
  del = require('del'),
  cssnano = require('cssnano');

gulp.task('copy', () => {
  return gulp.src([src + '**/*.*', '!' + src + '**/*.scss'], { base: src })
    .pipe(newer(dist))
    .pipe(gulp.dest(dist));
});

gulp.task('scss', () => {
  const
    processors = [
      autoprefixer,
      cssnano
    ];
  return gulp.src(css.src)
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss(processors))
    .pipe(gulp.dest(css.dist))
});

gulp.task('watch', () => {
  gulp.watch('src/**/*.*', gulp.series('copy', 'scss')).on('unlink', file => {
    del.sync(file.replace(/\\/g, '/').replace(src, dist).replace(/scss/g, 'css'), {   //Windows returns \
      force: true
    });
  });
});